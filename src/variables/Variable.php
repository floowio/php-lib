<?php

namespace Floowio\Variables;

abstract class Variable
{
    protected $name = '';

    protected $type = null;

    protected $value = null;

    protected $description = '';

    public function __construct($name, $value, $description = '')
    {
        $this->name = $name;
        $this->value = $value;
        $this->description = $description;
    }

    public function __get($name)
    {
        if (!property_exists(static::class, $name))
            throw new \InvalidArgumentException("Property {$name} does not exist");

        return $this->$name;
    }

    public function toArray()
    {
        return [
            'name'          => $this->name,
            'type'          => $this->type,
            'value'         => $this->value,
            'description'   => $this->description
        ];
    }

    /// FACTORY METHODS

    public static function number($name, $value = 0, $description = '')
    {
        return new NumberVariable($name, $value, $description);
    }

    public static function text($name, $value = '', $description = '')
    {
        return new TextVariable($name, $value, $description);
    }

    public static function boolean($name, $value = false, $description = '')
    {
        return new BooleanVariable($name, $value, $description);
    }

    public static function date($name, $value = null, $description = '')
    {
        return new DateVariable($name, $value, $description);
    }

    public static function enum($name, $value = '', $description = '')
    {
        return new EnumVariable($name, $value, $description);
    }
}