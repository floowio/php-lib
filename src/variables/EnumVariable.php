<?php

namespace Floowio\Variables;

class EnumVariable extends Variable
{
    protected $options = [];

    public function __construct($name, $value, $description = '')
    {
        parent::__construct($name, $value, $description);
        $this->type = 'enum';
    }

    public function option($value, $label)
    {
        $this->options[$value] = $label;
        return $this;
    }

    public function options(array $options)
    {
        foreach ($options as $value => $label)
            $this->option($value, $label);

        return $this;
    }

    public function toArray()
    {
        $options = [];
        foreach ($this->options as $value => $label) {
            $options[] = [ 'value' => $value, 'label' => $label ];
        }

        return array_merge(parent::toArray(), [
            'options' => $options
        ]);
    }
}