<?php

namespace Floowio\Variables;

class NumberVariable extends Variable
{
    public function __construct($name, $value, $description = '')
    {
        if (empty($value))
            $value = 0;

        if (!is_numeric($value))
            throw new \InvalidArgumentException('Default value has to be numeric');

        parent::__construct($name, $value, $description);
        $this->type = 'number';
    }
}