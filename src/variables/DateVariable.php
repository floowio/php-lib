<?php

namespace Floowio\Variables;

class DateVariable extends Variable
{
    public function __construct($name, $value, $description = '')
    {
        if (empty($value))
            $value = null;

        if (is_int($value))
            $value = (new \DateTime())->setTimestamp($value);

        if (!($value instanceof \DateTime) && $value !== null)
            throw new \InvalidArgumentException('Given value must be instance of date, timestamp or null');

        parent::__construct($name, $value, $description);
        $this->type = 'date';
    }

    public function toArray()
    {
        return array_merge(parent::toArray(), [
            'value' => $this->value instanceof \DateTime ? $this->value->format('c') : null
        ]);
    }
}