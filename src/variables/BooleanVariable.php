<?php

namespace Floowio\Variables;

class BooleanVariable extends Variable
{
    public function __construct($name, $value, $description = '')
    {
        parent::__construct($name, !!$value, $description);
        $this->type = 'bool';
    }
}